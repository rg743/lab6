package echoserver;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.Scanner;

public class EchoServer 
{

    public static void main(String[] args) throws IOException
    {
        final int _port = 1523; //Integer.parseInt(args[0]);
            boolean _open = true;
            String _line;
            String _clientString;
        
        try
       {
            //creating a server socket on port number 8888
            ServerSocket serverSocket = new ServerSocket(_port);
            System.out.println("Server waiting for a client. . . ");
            
            while(true)
            {
                //waiting for a client. Creating a sicket to communicate with the client
                Socket myClient = serverSocket.accept();
                System.out.println("Client connected. . . ");

                //creating a scanner to read text from client
                Scanner scanner = new Scanner(new InputStreamReader(myClient.getInputStream()));
                
                //creating a printwriter to be able to send a welcome message to the client
                PrintWriter printer = new PrintWriter(myClient.getOutputStream(), true);
                printer.println("\r\nWelcome!! type 'exit' to quit. \r\n");
                printer.flush();
                
                do 
                {
                    _line = scanner.nextLine();
                    if (_line != null) 
                    {
                        _clientString = _line;
                        printer.println("Server received your message: " + _clientString);
                        printer.flush();
                        System.out.println(_clientString);
                    }
                    
                } 
                while (!_line.trim().equals("exit"));
                {
                    _open = false;
                    serverSocket.close();
                    
                }
                
            }
            
       }
       catch(IOException e)
       {
           System.out.println("Error: " + e.getMessage());
       }      
    }
}